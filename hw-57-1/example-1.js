
    const tasks = [
        {id: 234, title: 'Create user registration API', timeSpent: 4, category: 'Backend', type: 'task'},
        {id: 235, title: 'Create user registration UI', timeSpent: 8, category: 'Frontend', type: 'task'},
        {id: 237, title: 'User sign-in via Google UI', timeSpent: 3.5, category: 'Frontend', type: 'task'},
        {id: 238, title: 'User sign-in via Google API', timeSpent: 5, category: 'Backend', type: 'task'},
        {id: 241, title: 'Fix account linking', timeSpent: 5, category: 'Backend', type: 'bug'},
        {id: 250, title: 'Fix wrong time created on new record', timeSpent: 1, category: 'Backend', type: 'bug'},
        {id: 262, title: 'Fix sign-in failed messages', timeSpent: 2, category: 'Frontend', type: 'bug'},
    ];

    const tasks1 = tasks.filter((item) => item.category === tasks[1].category);

    let totalTime = tasks1.reduce((time, tasks1) => {
        time += tasks1.timeSpent;
        return time;
    }, 0);
    console.log('Task-1 : ' + totalTime);


    const tasks2 = tasks.filter((item) => item.type === tasks[6].type);

    let totalTime1 = tasks2.reduce((time1, tasks2) => {
        time1 += tasks2.timeSpent;
        return time1;
    }, 0);
    console.log('Task-2 : ' + totalTime1);


    const tasks3 = tasks.filter((item) => item.title === tasks[1].title || item.title === tasks[2].title);
    console.log('Task-3 : ' + tasks3.length);

    const tasks4 = tasks.filter((item) => item.category === tasks[0].category);


    console.log('Task-4 : ' + 'Frontend : ' + tasks1.length + '  Backend : ' + tasks4.length);


    const tasks5 = tasks.filter((item) => item.timeSpent > 4);
    const tasks6 = tasks5.map(item => ({title: item.title, category:item.category}));
    console.log('Task-5 :' + JSON.stringify(tasks6));






