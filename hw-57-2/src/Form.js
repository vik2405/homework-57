import React  from 'react';
import './Form.css';

const Form = (props) => {
    return(
    <div className="form">
        <input className="text" placeholder="Add name" type="text" value={props.itemInput} onChange={props.changeItemInput}/>
        <input  className="text2" type="number" placeholder="Add cost" value={props.costInput} onChange={props.changeCostInput}/>
        <button className="btn" onClick={props.addExpense}>ADD EXPENSE</button>
    </div>
)
};

export default Form;



