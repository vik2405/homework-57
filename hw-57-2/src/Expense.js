import React from 'react';
import './Expense.css'
const Expense = (props) => {
    return(
    <div className="expense">
        <div className="name">Name: {props.name}</div>
        <div className="cost">Cost (KGS): {props.cost}</div>
        <button className="btn2" onClick={props.remove}>X</button>
    </div>
    )
};

export default Expense;