import React, { Component } from 'react';
import './App.css';
import Form from "./Form";
import List from "./List";

class App extends Component {

    state = {
        itemInput: '',
        costInput: '',
        id: '',
        total: 0,
        expenses: []
    };

    changeItemInput = event => this.setState({itemInput: event.target.value});

    changeCostInput = event => this.setState({costInput: event.target.value});

    changeId = () => this.setState({id: Date.now()});


    addExpense = () => {
        const expenses = [...this.state.expenses];
        expenses.push({
            name: this.state.itemInput,
            cost: this.state.costInput,
            id: this.state.id,
        });

        let totalNewSum = expenses.reduce((totalSum, newTotal) => {
            totalSum += parseInt(newTotal.cost, 10);
            return totalSum;
        },0);


        this.setState({expenses, itemInput: '', costInput: '', total:totalNewSum })
    };

    removeExpense = (id) => {
        let removeExpense = [...this.state.expenses];
        let indexExpense = removeExpense.findIndex(expenses => expenses.id === id );
        removeExpense.splice(indexExpense, 1);
        let newTotal = this.state.total - this.state.costInput;
        this.setState({expenses: removeExpense, total: newTotal});
    };


    render() {
        return (
            <div className="App">
                <h2 className="title">PERSONAL EXPENSES</h2>
                <Form itemInput={this.state.itemInput}
                      changeItemInput={this.changeItemInput}
                      costInput={this.state.costInput}
                      changeCostInput={this.changeCostInput}
                      changeId={this.changeId}
                      addExpense={this.addExpense} />
                <List expenses={this.state.expenses} remove={this.removeExpense}   />
                <div className="total"> TOTAL SPENT(KGS) : {this.state.total}</div>

            </div>
        )
    }
}

export default App;

