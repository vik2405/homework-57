
import React from 'react';
import Expense from "./Expense";
const List = props => (
    <div className="list">
        { props.expenses.map((expense) => <Expense name={expense.name} cost={expense.cost} remove={() =>props.remove(expense.id)}  itemId={expense.id} key={expense.id} />)}
    </div>
);

export default List;